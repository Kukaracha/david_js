let resultObj = { currentState: 0 };

const minusNumber = () => {
    --resultObj.currentState;

    if (resultObj.currentState < 0) {
        resultObj.currentState = 10;
    }
};

const plusNumber = () => {
    ++resultObj.currentState;
    
    if (resultObj.currentState > 10) {
        resultObj.currentState = 0;
    }
};

document.addEventListener('keydown', e => {
    if(e.keyCode === 39) {

        resultObj.result4 = displayNumber(plusNumber(resultObj.currentState)); 
    }

    if(e.keyCode === 37) {

        resultObj.result5 = displayNumber(minusNumber(resultObj.currentState));
    }
});

const displayNumber = () => {
    
    document.getElementById('result').innerHTML = resultObj.currentState;
};


document.getElementById('plusButton').addEventListener('click', () => {
    
    resultObj.result1 = displayNumber(plusNumber(resultObj.currentState));
});

document.getElementById('minusButton').addEventListener('click', () => {

    resultObj.result2 = displayNumber(minusNumber(resultObj.currentState));
});

document.getElementById('creator').addEventListener('click', () => {

    resultObj.result3 = createNewCounterShell();
});

const createNewCounterShell = () => {
    const wrapper = document.createElement('wrapper');
    body.appendChild(wrapper);
    
    const buttons = document.createElement('div');
    buttons.setAttribute('class', "buttons");
    wrapper.appendChild(buttons);

    const minusButton = document.createElement('input');
    minusButton.setAttribute('value', "Уменьшить -1");
    minusButton.setAttribute('type', "button");
    minusButton.setAttribute('class', "button1");
    buttons.appendChild(minusButton);

    const plusButton = document.createElement('input');
    plusButton.setAttribute('value', "Увеличить +1");
    plusButton.setAttribute('type', "button");
    plusButton.setAttribute('class', "button2");
    buttons.appendChild(plusButton);

    const span = document.createElement('span');
    span.setAttribute('class', "bubble output");
    span.setAttribute('id', "result");
    wrapper.appendChild(span);
};

const manageResult = resultObj => {
    switch (resultObj) {
        case 'resultObj.result1':

            return resultObj.result1;
            break;
        case 'resultObj.result2':

            return resultObj.result2;
            break;
        case 'resultObj.result3':

            return resultObj.result3;
            break;
        case 'resultObj.result4':

            return resultObj.result4;
            break;
        case 'resultObj.result5':

            return resultObj.result5;
            break;
    }
};

document.addEventListener('DOMContentLoaded', manageResult);