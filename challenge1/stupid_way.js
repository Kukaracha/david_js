const numbersArray = () => {
    const array = [];
    
    for(let i = 0; i < 3; i++) {
        array.push(parseInt(document.getElementById('number' + (i + 1)).value));
    }

    return array;
};

const sortArray = array => {
    for (let j = 0; j < array.length; j++) {
        for (let i = 0; i < array.length - 1; i++) {
            if (array[i] > array[i + 1]) {
                let prev = array[i];
                array[i] = array[i + 1];
                array[i + 1] = prev;
            }
        }
    }

    const sortedArray = array;
    
    return sortedArray;
};

const calcMiddleNumber = sortedArray => {
    const middleNumberDisplay = document.getElementById('result');
    const median = sortedArray[(sortedArray.length / 2) - 0.5];
    
    if(sortedArray[0] == sortedArray[1] || sortedArray[1] == sortedArray[2] || sortedArray[0] == sortedArray[2]) {
        middleNumberDisplay.innerHTML = 'КЕК!';
        alert('Чел, вводи разные числа, лол :)');
    } else { 
        middleNumberDisplay.innerHTML = median;
    }

    return middleNumberDisplay;
};

const output = () => {   
    calcMiddleNumber(sortArray(numbersArray()));
};