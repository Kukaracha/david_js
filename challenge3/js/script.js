const minus = () => {
    let currentValue = parseInt(document.getElementById('result').innerText, 10);
       
    if(currentValue <= 0) {
        currentValue = 0;
        alert('This is to low bro. I can`t do that!');
    } else {
        currentValue--;
    }
    
    document.getElementById('result').innerHTML = currentValue;

    return currentValue;
};

const plus = () => {
    let currentValue = parseInt(document.getElementById('result').innerText, 10);
    
    currentValue++;
    document.getElementById('result').innerHTML = currentValue;

    return currentValue;
};