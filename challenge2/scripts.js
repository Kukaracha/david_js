// Making array of number from values in input

const getNumbers = () => {
    const inputNumbers = document.getElementById('numbers').value;
    let numbersArray = inputNumbers.split(',');
    
    for (let i = 0; i < numbersArray.length; i++) {
        numbersArray[i] = parseInt(numbersArray[i], 10);
    }

    return numbersArray;
};
    
const sortNumbers = numbersArray => {
    for (let j = 0; j < numbersArray.length; j++) {
        for (let i = 0; i < numbersArray.length - 1; i++) {
            if (numbersArray[i] > numbersArray[i + 1]) {
                let prev = numbersArray[i];
                numbersArray[i] = numbersArray[i + 1];
                numbersArray[i + 1] = prev;
            }
        }
    }

    const sortedArray = numbersArray;

    return sortedArray;
};
    
const calcMedian = sortedArray => {
    if (sortedArray.length % 2 === 0) {
        const middlePoint = sortedArray.length / 2;
        median = (sortedArray[middlePoint - 1] + sortedArray[middlePoint]) / 2;
    } else {
        median = sortedArray[(sortedArray.length / 2) - 0.5];
    }

    return median;
};

const showMedian = median => {
    const finalResult = document.getElementById('result');
    
    finalResult.innerHTML = median;
};

const output = () => {
    showMedian(calcMedian(sortNumbers(getNumbers())));
};



